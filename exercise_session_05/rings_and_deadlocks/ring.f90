!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!!    Skeleton program for Ex. 5.1:
!!    Rings and deadlocks
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program ring
  use mpi
  implicit none
  
  ! Variables declaration
  integer :: tmp,istatus(MPI_STATUS_SIZE), send,group, left_rank, right_rank, my_sum,i, my_rank, n_proc, ierror

  ! Begin MPI
  call MPI_INIT(ierror)
  
  ! Get my_rank and n_proc
  call MPI_COMM_RANK(MPI_COMM_WORLD, my_rank, ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, n_proc, ierror)
  
  left_rank = mod(my_rank - 1 + n_proc, n_proc)
  right_rank = mod(my_rank + 1, n_proc)

  group = mod(my_rank, 2)

  my_sum = 0
  send = my_rank

  if (group == 0) then
    do i = 1, n_proc, +1

      !write(*,*) 'rank=', my_rank, ' sends: ', send, ' to: ', right_rank
      call MPI_SSEND(send, 1, MPI_INTEGER, right_rank, 1, MPI_COMM_WORLD, ierror)
      
      call MPI_RECV(tmp, 1, MPI_INTEGER, left_rank, 1, MPI_COMM_WORLD, istatus, ierror)
      !write(*,*) 'rank=', my_rank, 'receives: ', tmp, 'from: ', left_rank
      my_sum = my_sum + tmp
      send = tmp

    end do

  else
    do i = 1, n_proc, +1

      call MPI_RECV(tmp, 1, MPI_INTEGER, left_rank, 1, MPI_COMM_WORLD, istatus, ierror)
      !write(*,*) 'rank=', my_rank, 'receives: ', tmp, 'from: ', left_rank
      my_sum = my_sum + tmp

      !write(*,*) 'rank=', my_rank, ' sends: ', send, ' to: ', right_rank
      call MPI_SSEND(send, 1, MPI_INTEGER, right_rank, 1, MPI_COMM_WORLD, ierror)
      send = tmp

    end do
  endif

  write(*,*) 'I am processor ',my_rank,' out of ',n_proc

  call MPI_FINALIZE (ierror)

end program ring