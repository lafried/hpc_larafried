#include "mpi.h"
#include <stdio.h>
#include <float.h>


int factorial(int n, int nProcs){
	if (n == 0 || n == 1) {return 1; }
	int result =1;
	int i = 0; 
	while (n>0 && i<nProcs){
		result = result*n;
		--n;
		++i;
	}
			return result; 
}

int main (int argc, char** argv)
{
	MPI_Init(NULL, NULL); 
    

	int nProcs;
	MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
	int myRank; 
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank); 
	
    int nr_terms = 10000000;
	
	int to_bcast;
    if (myRank == 0) {
      to_bcast = nr_terms/nProcs; 
    }
	
	MPI_Bcast(&to_bcast, 1, MPI_INT, 0,MPI_COMM_WORLD); 
	
	int n_iter = to_bcast;
	int n; 
	long double sum = 0.0; 
    unsigned long long prev_fact = 1;
    int part_fact;
	long double tmp; 

	for (int i= 0; i < n_iter; ++i){
		n = i*nProcs+myRank; 
		part_fact = factorial(n, nProcs); 
		tmp = 1.0/(prev_fact*part_fact); 
		sum = sum + tmp;
		prev_fact = part_fact;  
	}
    
 	
	long double e; 
	MPI_Reduce(&sum, &e, 1, MPI_LONG_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 

	
	
	if (myRank == 0){
      printf("e is approx.: %.*Lf\n", LDBL_DECIMAL_DIG, e);
	}
	MPI_Finalize(); 	
}


